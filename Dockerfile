#javaweb运行环境
FROM centos:6.6
MAINTAINER noday
WORKDIR /root/
RUN yum -y install wget
RUN yum update -y
RUN yum install -y java
RUN yum install -y tar
RUN yum install -y unzip
RUN yum install -y mysql-server
RUN curl -O http://apache.fayea.com/tomcat/tomcat-7/v7.0.63/bin/apache-tomcat-7.0.63.tar.gz
RUN tar zxvf apache-tomcat-7.0.63.tar.gz
RUN rm -rf /apache-tomcat-7.0.63/webapps/*
RUN wget http://222.73.85.164/zhao.zip
RUN unzip /root/zhao.zip -d /apache-tomcat-7.0.63/webapps/ROOT
ENV TOMCAT_HOME /apache-tomcat-7.0.63
ENV CATALINA_HOME /apache-tomcat-7.0.63
ENV LC_ALL en_US.UTF-8
ADD initapp.sh /initapp.sh
RUN chmod u+x /initapp.sh
RUN /initapp.sh
EXPOSE 8080
CMD $CATALINA_HOME/bin/catalina.sh run